// g++ -m32 -Wall -o 1.out 1.cpp -L"C:\Program Files (x86)\Dev-Cpp\MinGW32\bin" -lglu32 -lglut32 -lopengl32 -lstdc++
// ./1.out

// #include "stdafx.h"
#include <GL\glut.h>
#include <stdlib.h>

void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-100, 100, -100, 100);
}

void pintar(GLint x0, GLint y0)
{
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POINTS);
	glVertex2i(x0, y0);
	glEnd();
}
int operador(int num)
{
	return num < 0 ? -1 : (num > 0 ? 1 : 0); //operador ternario
}
void recta_punto_medio(GLint x0, GLint y0, GLint x1, GLint y1)
{
	float dE, dx, dy, temp;
	GLint s1, s2, intc, i, x, y;

	x = x0;
	y = y0;
	dx = abs(x1 - x0);
	dy = abs(y1 - y0);
	s1 = operador(x1 - x0);
	s2 = operador(y1 - y0);

	if (dy > dx)
	{
		temp = dx;
		dx = dy;
		dy = temp;
		intc = 1;
	}
	else
		intc = 0;

	dE = 2 * dy - dx;

	for (i = 1; i <= dx; i++)
	{
		pintar(x, y);
		if (dE >= 0)
		{
			intc == 1 ? x = x + s1 : y = y + s2;

			dE = dE - (2 * dx);
		}

		intc == 1 ? y = y + s2 : x = x + s1;

		dE = dE + 2 * dy;
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	recta_punto_medio(0, 0, 0, 20);	  //vertical
	recta_punto_medio(5, 0, 20, 0);	  //horizontal
	recta_punto_medio(5, 5, 20, 20);  //ICuadrante
	recta_punto_medio(25, 20, 40, 5); //IVCuadrante
	recta_punto_medio(42, 5, 50, 20); //II2Cuadrante

	glFlush();
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(600, 500);
	glutInitWindowPosition(200, 100);
	glutCreateWindow("My House");
	init();
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}
