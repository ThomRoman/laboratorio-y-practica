// g++ -m32 -Wall -o 1.out 1.cpp -L"C:\Program Files (x86)\Dev-Cpp\MinGW32\bin" -lglu32 -lglut32 -lopengl32 -lstdc++
// ./1.out

// #include "stdafx.h"
#include <GL\glut.h>
#include <Math.h>

void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-100, 100, -100, 100);
}

void pintar(GLint x0, GLint y0)
{
	glColor3f(1.0, 1.0, 0.0);
	// glPointSize(5);
	glBegin(GL_POINTS);
	glVertex2i(x0, y0);
	glEnd();
}

int recta_simple(GLint x0, GLint y0, GLint x1, GLint y1)
{

	float dx, dy, m, b, y;
	dx = x1 - x0;
	dy = y1 - y0;

	m = dy / dx;

	b = y0 - m * x0;

	y = y0;

	for (int x = x0; x <= x1; x++)
	{
		pintar(x, y);
		y = m * x + b;
	}

	return 0;
}

void recta_punto_medio(GLint x0, GLint y0, GLint x1, GLint y1)
{

	GLint dx, dy, dE, dNE, d, x, y;
	dx = x1 - x0;
	dy = y1 - y0;
	d = 2 * dy - dx;
	dE = 2 * dy;
	dNE = 2 * (dy - dx);
	x = x0;
	y = y0;
	pintar(x, y);

	while (x < x1)
	{
		if (d <= 0)
		{
			d += dE;
			x++;
		}
		else
		{
			d += dNE;
			x++;
			y++;
		}
		pintar(x, y);
	}
}
void recta_dda(GLint x0, GLint y0, GLint x1, GLint y1)
{

	float dx, dy, m;
	GLint y;
	dx = x1 - x0;
	dy = y1 - y0;
	m = dy / dx;
	y = y0;
	for (int x = x0; x <= x1; x++)
	{
		pintar(x, y);
		y += m;
	}
}
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	recta_simple(0, 0, 20, 20);
	recta_punto_medio(5, 0, 25, 20);
	recta_dda(10, 0, 30, 20);

	glFlush();
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(600, 500);
	glutInitWindowPosition(200, 100);
	glutCreateWindow("My House");
	init();
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}
