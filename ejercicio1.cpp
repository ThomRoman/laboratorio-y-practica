//uso de Traslacion en OpenGL: gtraslatef / glTraslated / glrotatef / glscalef
#include <GL/glut.h>
#include <iostream>
using namespace std;

void inicio()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); //cambia la matriz que estamos usando por la matriz identidad
  glOrtho(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
  glClearColor(0.0, 0.0, 0.0, 0.0);
}

void diseno()
{
  glClear(GL_COLOR_BUFFER_BIT); // limpiar pantalla

  //dibujar base del barco
  glColor3f(0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  glVertex2f(-7.0, -4.0); // primer vertice
  glVertex2f(-4.0, -4.0);
  glVertex2f(-2.0, -2.5);
  glVertex2f(-9.0, -2.5);
  glEnd();

  //dibujar palo del barco
  glColor3f(0.0, 1.0, 0.0);
  glBegin(GL_QUADS);
  glVertex2f(-5.4, -2.5); // primer vertice
  glVertex2f(-5.6, -2.5);
  glVertex2f(-5.6, 5.0);
  glVertex2f(-5.4, 5.0);
  glEnd();

  //dibujar asta derecha del barco
  glColor3f(1.0, 1.0, 0.0);
  glBegin(GL_TRIANGLES);
  glVertex2f(-5.35, -2.2); // primer vertice
  glVertex2f(-2.7, -2.2);
  glVertex2f(-5.35, 4.2);
  glEnd();

  //dibujar asta izquierda del barco
  glColor3f(1.0, 0.0, 1.0);
  glBegin(GL_TRIANGLES);
  glVertex2f(-5.65, -2.2); // primer vertice
  glVertex2f(-8.3, -2.2);
  glVertex2f(-5.65, 2.7);
  glEnd();

  glFlush(); // obliga a ejecutar los comandos anteriores
}

void trasladar()
{
  diseno();
  glPushMatrix();
  glTranslatef(8.0, 0.0, 3.0);
  diseno();
  glPopMatrix();
}

void mostrarpantalla()
{
  int opcion;
  diseno();
  //do{
  cout << "[1].Trasladar \n [2]Salir \n";
  cout << "ingrese una opcion: ";
  cin >> opcion;
  switch (opcion)
  {
  case 1:
    trasladar();
    break;
  }
}
// }while(opcion!=4);}

int main(int argc, char **argv)
{
  glutInit(&argc, argv); // inicia GLUT
  glutCreateWindow("Transformaciones OPENGL");
  glutInitWindowSize(600, 450);
  glutInitWindowPosition(50, 50);
  glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
  inicio();
  glutDisplayFunc(mostrarpantalla);
  glutMainLoop();
  return 0;
}
