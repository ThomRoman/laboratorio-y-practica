#include <windows.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>

using namespace std;

void iniciarProyeccion()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-100, 100, -100, 100);
}

void dibujarLinea(GLint x0, GLint y0, GLint xf, GLint yf)
{
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0);
	glVertex2i(x0, y0);
	glVertex2i(xf, yf);
	glEnd();
}

void setPixel(GLint x, GLint y)
{
	glColor3f(1.0, 0.0, 0.0);
	glPointSize(5);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

int signo(int num)
{
	int resultado;

	if (num < 0)
	{
		resultado = -1;
	}
	if (num > 0)
	{
		resultado = 1;
	}
	if (num == 0)
	{
		resultado = 0;
	}
	return (resultado);
}
void basico(GLint x0, GLint y0, GLint xf, GLint yf)
{
	int x;
	float dx, dy, m, y, b;

	dx = xf - x0;
	dy = yf - y0;

	m = dy / dx;
	b = y0 - m * x0;
	y = y0;

	for (x = x0; x <= xf; x++)
	{
		setPixel(x, y);
		y = m * x + b;
	}
}

void bresenham(GLint x0, GLint y0, GLint xf, GLint yf)
{
	float e, dx, dy, temp;
	GLint s1, s2, intercambio, i, x, y;

	x = x0;
	y = y0;
	dx = abs(xf - x0);
	dy = abs(yf - y0);
	s1 = signo(xf - x0);
	s2 = signo(yf - y0);

	if (dy > dx)
	{
		temp = dx;
		dx = dy;
		dy = temp;
		intercambio = 1;
	}
	else
	{
		intercambio = 0;
	}

	e = 2 * dy - dx;

	for (i = 1; i <= dx; i++)
	{
		setPixel(x, y);
		if (e >= 0)
		{
			if (intercambio == 1)
			{
				x = x + s1;
			}
			else
			{
				y = y + s2;
			}
			e = e - (2 * dx);
		}
		if (intercambio == 1)
		{
			y = y + s2;
		}
		else
		{
			x = x + s1;
		}
		e = e + 2 * dy;
	}
}

void dda(GLint x0, GLint y0, GLint xf, GLint yf)
{
	GLint i;
	float x, y, ax, ay, res;

	if (abs(xf - x0) >= abs(yf - y0))
	{
		res = abs(xf - x0);
	}
	else
	{
		res = abs(yf - y0);
	}
	ax = (xf - x0) / res;
	ay = (yf - y0) / res;

	i = 1;
	x = (float)x0;
	y = (float)y0;

	while (i <= res)
	{

		setPixel(roundf(x), roundf(y));
		x = x + ax;
		y = y + ay;
		i++;
	}
}

void draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	int opcion;
	cout << " [1].Basico \n [2].Bresenham \n [3].DDA \n [4].Salir \n";
	cout << "ingrese una opcion: ";
	cin >> opcion;
	switch (opcion)
	{
	case 1:
		basico(-5, -5, 15, 10);
		// dibujarLinea(-5,-5,15,10);
		break;
	case 2:
		bresenham(-5, -5, -5, 15);
		dibujarLinea(-5, -5, -5, 15);
		break;
	case 3:
		dda(-5, -5, -15, 15);
		dibujarLinea(-5, -5, -15, 15);
		break;
	}
	glFlush();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(300, 100);
	glutCreateWindow("Pregunta 1");
	iniciarProyeccion();
	glutDisplayFunc(draw);
	glutMainLoop();
}
