#include <windows.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>

using namespace std;

void iniciarProyeccion()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-100, 100, -100, 100);
}

void dibujarLinea(GLint x0, GLint y0, GLint xf, GLint yf)
{
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0);
	glVertex2i(x0, y0);
	glVertex2i(xf, yf);
	glEnd();
}

void setPixel(GLint x, GLint y)
{
	glColor3f(1.0, 0.0, 0.0);
	glPointSize(5);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

int signo(int num)
{
	int resultado;

	if (num < 0)
	{
		resultado = -1;
	}
	if (num > 0)
	{
		resultado = 1;
	}
	if (num == 0)
	{
		resultado = 0;
	}
	return (resultado);
}

void bresenham(GLint x0, GLint y0, GLint xf, GLint yf)
{
	float e, dx, dy, temp;
	GLint s1, s2, intercambio, i, x, y;

	x = x0;
	y = y0;
	dx = abs(xf - x0);
	dy = abs(yf - y0);
	s1 = signo(xf - x0);
	s2 = signo(yf - y0);

	if (dy > dx)
	{
		temp = dx;
		dx = dy;
		dy = temp;
		intercambio = 1;
	}
	else
	{
		intercambio = 0;
	}

	e = 2 * dy - dx;

	for (i = 1; i <= dx; i++)
	{
		setPixel(x, y);
		if (e >= 0)
		{
			if (intercambio == 1)
			{
				x = x + s1;
			}
			else
			{
				y = y + s2;
			}
			e = e - (2 * dx);
		}
		if (intercambio == 1)
		{
			y = y + s2;
		}
		else
		{
			x = x + s1;
		}
		e = e + 2 * dy;
	}
}

void linea(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	bresenham(-55, -25, -55, 25);
	dibujarLinea(-55, -25, -55, 25);

	bresenham(-50, -25, 0, -25);
	dibujarLinea(-50, -25, 0, -25);

	bresenham(-45, -15, -5, 20);
	dibujarLinea(-45, -15, -5, 20);

	bresenham(5, 20, 45, -15);
	dibujarLinea(5, 20, 45, -15);

	bresenham(45, -20, 85, 15);
	dibujarLinea(45, -20, 85, 15);

	glFlush();
}

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(300, 100);
	glutCreateWindow("DDA line");
	iniciarProyeccion();
	glutDisplayFunc(linea);
	glutMainLoop();
}
