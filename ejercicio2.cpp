//uso de Traslacion en OpenGL: gtraslatef / glTraslated / glrotatef / glscalef
#include <GL/glut.h>
#include <iostream>
#include <math.h>
using namespace std;

void inicio()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); //cambia la matriz que estamos usando por la matriz identidad
  glOrtho(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
  glClearColor(0.0, 0.0, 0.0, 0.0);
}

void diseno()
{
  glClear(GL_COLOR_BUFFER_BIT); // limpiar pantalla

  //dibujar asta izquierda del barco
  glColor3f(1.0, 0.0, 1.0);
  glBegin(GL_TRIANGLES);
  glVertex2f(2.0, 1.0); // primer vertice
  glVertex2f(7.0, 2.0);
  glVertex2f(6.0, 6.0);
  glEnd();

  glFlush(); // obliga a ejecutar los comandos anteriores
}

void rotar()
{
  diseno();
  glPushMatrix();
  glRotatef(90, 0.0, 0.0, 1.0);
  diseno();
  glPopMatrix();
}

void rotar2()
{
  diseno();
  glPushMatrix();
  glRotatef(180, 0.0, 0.0, 1.0);
  diseno();
  glPopMatrix();
}

void mostrarpantalla()
{
  int opcion;
  diseno();

  do
  {
    cout << "[1].Rotar primera vez \n [2].Rotar por segunda vez \n [3]Salir \n";
    cout << "ingrese una opcion: ";
    cin >> opcion;

    switch (opcion)
    {
    case 1:
      rotar();
      break;
    case 2:
      rotar2();
      break;
    }
  } while (opcion != 3);
}

int main(int argc, char **argv)
{
  glutInit(&argc, argv); // inicia GLUT
  glutCreateWindow("Transformaciones OPENGL");
  glutInitWindowSize(600, 450);
  glutInitWindowPosition(50, 50);
  glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
  inicio();
  glutDisplayFunc(mostrarpantalla);
  glutMainLoop();

  return 0;
}
